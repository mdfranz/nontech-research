# NRA
https://medium.com/@LaddEveritt/from-russia-with-love-for-the-nra-ffc69088fe41

# Russia Ties

## 2019
- https://www.thedailybeast.com/the-gop-is-the-russian-propaganda-party-now
- https://www.theatlantic.com/ideas/archive/2019/12/false-romance-russia/603433/

## 2017
https://www.politico.com/magazine/story/2017/02/how-russia-became-a-leader-of-the-worldwide-christian-right-214755

## 2016
- https://www.theatlantic.com/politics/archive/2016/12/the-conservative-split-on-russia/510317/

## 2014
- https://theimaginativeconservative.org/2014/08/whose-side-god-now-vladimir-putin.html
- https://www.salon.com/2019/12/14/russia-and-the-republicans-how-vladimir-putin-got-an-american-subsidiary/
- https://thehill.com/blogs/pundits-blog/religion/202866-god-and-putin-pat-buchanans-startling-insight

## 2013
- https://www.businessinsider.com/is-vladimir-putin-a-us-style-conservative-2013-12
- https://www.irishcentral.com/opinion/cahirodoherty/pat-buchanan-supports-russian-crackdown-on-gays-219554871-238249321
